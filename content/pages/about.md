---
title: About Me
---

* **Professional Experience:**
	* **The Church Online** (2017-Present) - *Manager - Technical Support*
	* **Catholic Cemeteries Association** (2016-2017) - *Sales Counselor*
	* **Metz Culinary Management** (2007-2016) - *General Manager/Chef*
	* **Pittsburgh Theological Seminary** (2009-2010) - *ILL Librarian*
	* **Reformed Presbyterian Seminary** (2010) - *Library Intern*
	* **Domino's Pizza** (2006-2007) - *Assistant Manager*
* **Education:**
	* **University of Pittsburgh** - *Masters in Library and Information Science*
	* **Pittsburgh Theological Seminary** - *Master of Arts*
	* **Lee University** - *Bachelor of Arts*
* **Skills**
	* Windows 10/7/Vista/XP	
	* Microsoft Office
	* HTML/CSS
	* Linux (Debian/Ubuntu, CentOS, & OpenSUSE)
	* Virtualization (Docker, Virtualbox, & LXC)
	* Python 3
	* JavaScript
  	* cPanel/WHM Hosting
 	* GSuite Administration
 	* Nginx Administration
