---
title: Installing EW-7711ulc on Debian Jessie.
tags: wifi, jessie, ew-7711ulc
category: Tutorials
---

A few months ago, I was having issues with my home network. Moving more than 10 feet away from the router would drop my connection percentage down to under 50%, which means I was connecting at less than half of the already slow 56Mb connection. I've grown used to Gb ethernet from when I only had desktop boxes, so I can only stand wireless data transfers with small files in small numbers. Anyone with even a small understanding of wireless networking can problem guess what the issue was.... interference from nearby networks. I live in a small town and the houses are very close together, in addition to that, a lot of the nearby houses have been converted to multiple apartment units in each house, therefore, the spectrum is just overrun with everyone's own personal routers. From my apartment, I can easily scan close to 35 different SSIDs!

## THE SOLUTION

The solution was pretty obvious, find a way to make my wifi network a little more "unique". I was already utilizing a good channel, switching to the less occupied channel 1 (those poor souls on channel 6 were all stuck messing with each other), but it still was crowded. So I decided to join the rest of the modern tech world and invest in some dual band equipment. The only thing I hate more than slow and inconsistent wifi are giant dongles hanging off of my tiny 11" laptop. I bought this thing small for a reason, I don't want to have to struggle with a usb interface that is nearly the same height as my keyboard. I found the Edimax EW-7711ulc. As far as I can tell, its the only "nano" sized device capable of the 5ghz band. I have not found a single dual band nano adapter in my research. I figured I already had 2.4ghz built in to the laptop, so I really just needed an interface for 5ghz. So I picked one up.

## THE SECOND PROBLEM

Unfortunately, Debian (and as far as I can tell, no version of Linux) currently supports the EW-7711ulc out of the box. I can build things from source, that's not an issue and something you actually get pretty used to when running Debian Stable. Anyways, Edimax does provide the driver on their website for Linux. Downloaded it, went to compile it, and..... no dice. It kept giving me this very vague error that I couldn't make heads or tails of. This doesn't appear to be a very popular chipset either, so I couldn't get much info on fixing the issue. 

## THE SECOND SOLUTION

I did however, come across a [bitbucket repo](https://bitbucket.org/sanrath/mediatek_mt7610u_sta_driver_linux-64bit) that sported a different driver for the same chipset (the MediaTek mt7610u). I gave it a shot and it worked immediately!

After solving that issue, I just updated my wireless interface in wicd-curses to reflect the new wireless adapter and was able to find my 5ghz SSID immediately. Now my home wifi network flies and I do not have to worry about interference from my neighbors at all!
